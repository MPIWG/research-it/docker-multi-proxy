# Docker proxy for multiple containers

This is a Docker container using docker-compose that provides a common proxy service for separate docker-compose based setups.

The proxy
* uses [Traefik 2](https://github.com/containous/traefik/)
* provides HTTP and HTTPS endpoints
* uses docker labels and shared docker network `proxy-net`

# Configuration

Create config file `traefik.yml` from `traefik.yml.template`.

The current config writes Traefik's operation log and the access log as files in `./log`. 
You can set `accesslog: {}` in `traefik.yml` to log to the container stdout. 

# Running

Start proxy container with
```
docker-compose up -d
```

## Requirements for proxied containers

Endpoint services that should be proxied need to declare [Traefik-specific labels](https://docs.traefik.io/reference/dynamic-configuration/docker/) and use the `proxy-net` network:
```yml
service:
  my-service:
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=proxy-net"
      - "traefik.http.routers.mysite.rule=Host(`${VIRTUAL_HOST}`)"
    network:
      - proxy-net
      
networks:
  proxy-net:
    name: proxy-net
```

For services using HTTPS you can provide certificate files in `./certs` and a config file listing all certficate files in `./config` (see `./config/traefik-certs.yml.template`). Traefik will automatically use the certificate for the `Host()` defined above. You only need to add the following labels:
```yml
labels:
  - "traefik.http.routers.mysite.entrypoints=https"
  - "traefik.http.routers.mysite.tls=true"
```

You can also use an automatically generated [Let's Encrypt](https://letsencrypt.org) certificate using the `leresolver` by adding the following labels
```yml
labels:
  - "traefik.http.routers.mysite.entrypoints=https"
  - "traefik.http.routers.mysite.tls=true"
  - "traefik.http.routers.mysite.certresolver=myresolver"
```
(see https://doc.traefik.io/traefik/v2.4/https/acme/)
